import 'package:dart_animal/dart_animal.dart' as dart_animal;

void main() {
  Alligator alligator1 = Alligator();
  alligator1.hunt();

  Crocodile crocodile1 = Crocodile();
  crocodile1.hunt();

  Cat cat1 = Cat();
  cat1.hunt();

  Bear bear1 = Bear();
  bear1.hunt();

  Parrots parrots1 = Parrots();
  parrots1.hunt();

  Vulture vulture1 = Vulture();
  vulture1.hunt();
}

mixin Swim {
  void swim() => print('Swimming');
}

mixin Bite {
  void bite() => print('Chomp');
}

mixin Crawl {
  void crawl() => print('Crawling');
}

mixin Rush {
  void rush() => print('Rushing');
}

mixin Catch {
  void catching() => print('Catching');
}

mixin Fly {
  void fly() => print('Flying');
}

mixin Hover {
  void hover() => print('Hovering');
}

abstract class Animal {
  void hunt() {
    print('\n Animal -------');
    print('Eat');
  }
}

abstract class AquaticsAnimal extends Animal with Swim, Crawl implements Bite {
  void behavior() {
    swim();
    crawl();
    bite();
  }

  @override
  void bite() => print('Chomp');
}

class Alligator extends AquaticsAnimal {
  @override
  void hunt() {
    print('\n Alligator -------');
    behavior();
    print('Eat Fish');
  }
}

class Crocodile extends AquaticsAnimal {
  @override
  void hunt() {
    print('\n Crocodile -------');
    behavior();
    print('Eat Zebra');
  }

  @override
  void bite() {}
}

abstract class LandAnimal extends Animal with Rush, Catch implements Bite {
  void behavior() {
    rush();
    catching();
    bite();
  }

  @override
  void bite() => print('Chomp');
}

class Cat extends LandAnimal {
  @override
  void hunt() {
    print('\n Cat -------');
    behavior();
    print('Eat Fish');
  }
}

class Bear extends LandAnimal {
  @override
  void hunt() {
    print('\n Bear -------');
    behavior();
    print('Eat Deer');
  }
}

abstract class PoultryAnimal extends Animal with Fly, Hover implements Bite {
  void behavior() {
    fly();
    hover();
    bite();
  }

  @override
  void bite() => print('Chomp');
}

class Parrots extends PoultryAnimal {
  @override
  void hunt() {
    print('\n Parrots -------');
    behavior();
    print('Eat Worm');
  }
}

class Vulture extends PoultryAnimal {
  @override
  void hunt() {
    print('\n Vulture -------');
    behavior();
    print('Eat Carrion');
  }
}
